package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/ChubbsSolutions/hnbot/objects"
)

const version = "0.1"

func main() {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/v1/top", getTop)
	log.Printf("Starting up hnbot %v...\n", version)
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	log.Fatal(http.ListenAndServe(":"+port, router))

	log.Print("Server started on port " + port)
}

//getTop
func getTop(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	word := r.URL.Query().Get("text")

	slackUser := r.URL.Query().Get("user_name")
	slackChannel := r.URL.Query().Get("channel_name")
	slackTeam := r.URL.Query().Get("team_id")
	log.Print("Request received for " + word + " from " + slackUser + ", from team " + slackTeam + ", on channel " + slackChannel)

	story, err := getTopStory()
	if fmt.Sprintf("%s", err) == "NOTFOUND" {
		log.Println("No stories found.")
		w.WriteHeader(http.StatusNotFound)

		response := objects.Response{Response: "No Stories Found.", BotVersion: version}
		resp, err1 := json.Marshal(response)
		if err1 != nil {
			log.Println("Error Marshalling response!")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Write(resp)
		return
	}

	if err != nil {
		log.Printf("Error - %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	log.Print("Returning story " + fmt.Sprint(story.ID) + " to " + slackUser + " from team " + slackTeam + " on channel " + slackChannel)

	w.WriteHeader(http.StatusOK)
	response := objects.SlackResponse{}
	response.Text = fmt.Sprintf("📰 %v :arrow_right: %v", story.Title, story.URL)
	response.ResponseType = "in_channel"
	response.BotVersion = version

	resp, err := json.Marshal(response)
	if err != nil {
		respBad, err := json.Marshal(response)
		if err != nil {
			log.Println("Error Marshalling response!")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Write(respBad)
		return

	}
	w.Write(resp)
}

//getTopStory get HN's top story
func getTopStory() (objects.StoryData, error) {
	var stories []int
	var story objects.StoryData
	var good = false

	for good == false {
		resp, err := http.Get("https://hacker-news.firebaseio.com/v0/topstories.json")
		if err != nil {
			return story, err
		}
		defer resp.Body.Close()

		data, _ := ioutil.ReadAll(resp.Body)
		if err != nil {
			return story, err
		}

		err = json.Unmarshal([]byte(data), &stories)
		if err != nil {
			return story, err
		}

		//Loop over the top stories and get the number one if possible
		for _, id := range stories {
			storyURL := fmt.Sprintf("https://hacker-news.firebaseio.com/v0/item/%v.json", id)
			fmt.Println(storyURL)
			resp, err := http.Get(storyURL)
			if err != nil {
				return story, err
			}
			defer resp.Body.Close()

			data, _ := ioutil.ReadAll(resp.Body)
			if err != nil {
				return story, err
			}

			err = json.Unmarshal([]byte(string(data)), &story)
			if err != nil {
				return story, err
			}
			if story.Dead == false && story.Deleted == false {
				return story, nil
			}
		}
	}
	return story, errors.New("NOT_FOUND")
}
