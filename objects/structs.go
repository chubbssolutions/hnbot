package objects

//Response is Slack's response to the server
type Response struct {
	Response   string `json:"response"`
	BotVersion string `json:"bot_version"`
}

//StoryData represents the JSON struct sent by Urban Dictionary with the word.
type StoryData struct {
	By          string `json:"by"`
	Deleted     bool   `json:"bool"`
	Text        string `json:"text"`
	Dead        bool   `json:"dead"`
	Descendants int    `json:"descendants"`
	ID          int    `json:"id"`
	Kids        []int  `json:"kids"`
	Score       int    `json:"score"`
	Time        int    `json:"time"`
	Title       string `json:"title"`
	Type        string `json:"type"`
	URL         string `json:"url"`
}

//SlackResponse is Slack's response to the server
type SlackResponse struct {
	Text         string `json:"text"`
	ResponseType string `json:"response_type"`
	BotVersion   string `json:"bot_version"`
}
