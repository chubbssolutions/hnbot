hnbot
==

Get your [Hacker News](https://news.ycombinator.com/news) on Slack.

Download the software
--
[Download](https://github.com/iarenzana/hnbot/releases) the latest version of hnbot for all major platforms.

Compile and run the source
--
Requires Go 1.5 or newer (earlier versions untested). Remember to set the GOPATH variable.

```
git clone https://gitlab.com:chubbssolutions/hnbot.git
cd hnbot
go get
go build
PORT=60001 ./hnbot.go
```

Usage
--
Run this service in Heroku (Procfile provided). Go to your Custom Integrations, Slash Commands on Slack and create a GET that points to https://[YOUR_HOST]:60001/v1/top.

About
--
Crafted with :heart: in Indiana by [Chubbs Solutions] (http://chubbs.solutions).
